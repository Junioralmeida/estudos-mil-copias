/** @type {import('tailwindcss').Config} */

import { fontFamily as _fontFamily } from 'tailwindcss/defaultTheme';

export const content = ['content/**/*.md', 'content/**/*.html', 'layouts/**/*.html'];
export const theme = {
  extend: {
    backgroundImage: {
      hero: "url('../img/bg-homepage.jpg')",
    },
  },
  fontFamily: {
    syne: ['"Syne"', ..._fontFamily.sans],
    poppins: ['"Poppins"', ..._fontFamily.sans],
  },
  colors: {
    gray: '#4B4846',
    white: '#fff',
    purple: '#8401FF',
    purpleLight: '#F9EFFE',
    blue: '#007AFF',
  },
};
export const plugins = [];
